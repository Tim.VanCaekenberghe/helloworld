package string.generators;
import static java.util.Arrays.stream;
import static org.junit.jupiter.api.Assertions.*;

import String.Generators.StringGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class GeneratorTest {


    @Test
    public void testRandomIntLowerThanMaxAndGreaterThanMin() {
        int x = StringGenerator.getRandomInt(0, 3);
        assertTrue((x < 3) && (x >= 0));
    }

    @Test
    public void testThrowsExceptionMaxSmallerThanMin() {
        assertThrows(IllegalArgumentException.class, () -> {
            StringGenerator.getRandomInt(0,-10);
        });
    }

    @Test
    public void testThrowsExceptionMaxEqualsToMin() {
        assertThrows(IllegalArgumentException.class, () -> {
            StringGenerator.getRandomInt(0,0);
        });
    }

    @Test
    public void testGetRandomFirstNameIsElementOfArrayFirstNames() {
        String first = StringGenerator.getRandomFirstName();
        String test = Arrays.stream(StringGenerator.getFirstNames()).filter(s -> first.equals(s)).findFirst().get();
        assertEquals(first, test);
    }


    @Test
    public void testGetRandomLastNameIsElemenetOfArrayFirstNames() {
        String last = StringGenerator.getRandomLastName();
        String test = Arrays.stream(StringGenerator.getLastNames()).filter(s -> last.equals(s)).findFirst().get();
        assertEquals(last, test);
    }

    @Test
    public void testFirstNameIsNotNullOrEmpty() {
        String x = StringGenerator.getRandomFirstName();
        assertFalse(x == null || x.isEmpty());
    }

    @Test
    public void testLastNameIsNotNullOrEmpty() {
        String x = StringGenerator.getRandomLastName();
        assertFalse(x == null || x.isEmpty());
    }




    /*@Test
    public void testEmptyFirstOrLastNameThrowsException() throws EmailMissingNameException {
        Assertions.assertThrows(EmailMissingNameException.class, () -> {
            String x = StringGenerator.email(" ", " ");
        });
    }
*/
    @Test
    public void testPasswordLengthIsGivenLength() {
        String password = StringGenerator.password(5);
        assertEquals(5, password.length());
    }

    @Test
    public void testPasswordContains1digit(){


    }
    public void ikHebAanpassingenGedaan() {

    }
}

